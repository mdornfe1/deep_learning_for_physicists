import os
from subprocess import call, Popen

import dot2tex
from graphviz import Digraph

dot = Digraph()

dot.node('W1', '$\\textbf{W}_1$')
dot.node('W2', '$\\textbf{W}_2$')

dot.node('h0', '$\\textbf{x}$', lblstyle='align=center')
dot.node('z1', 'op = matmul \\\\\ $\\textbf{z}_1=\\textbf{W}_1\\textbf{x}$', lblstyle='align=center')
dot.node('a1', 'op = $\sigma$ \\\\\ $\\textbf{a}_1=\sigma(\\textbf{z}_1)$', lblstyle='align=center')
dot.node('z2', 'op = matmul \\\\\ $\\textbf{z}_2=\\textbf{W}_2\\textbf{a}_1$', lblstyle='align=center')
dot.node('a2', 'op = $\sigma$ \\\\\ $\\hat{\\textbf{y}}=\sigma(\\textbf{z}_2)$', lblstyle='align=center')


dot.edge('h0', 'z1')
dot.edge('W1', 'z1')
dot.edge('z1', 'a1')
dot.edge('a1', 'z2')
dot.edge('W2', 'z2')
dot.edge('z2', 'a2')

texcode = dot2tex.dot2tex(dot.source, format='tikz', texmode='raw', crop=True, autosize=True)
dir_path = os.path.dirname(os.path.realpath(__file__))
base_name = __file__.split(os.sep)[-1].replace('.py', '')
tex_file = os.path.join(dir_path, '{}.tex'.format(base_name))

with open(tex_file, 'w') as f:
	f.write(texcode)
	proc = Popen(['pdflatex', '-output-directory={}'.format(dir_path), tex_file])

proc.communicate()

rm_files = [os.path.join(dir_path, base_name + suffix)  for suffix in ['.aux', '.tex', '.log']]
call(['rm'] + rm_files)