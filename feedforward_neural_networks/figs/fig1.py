import os
from subprocess import call, Popen

import dot2tex
from graphviz import Digraph

dot = Digraph()

dot.node('h0', 'state = $\\textbf{x}$', lblstyle='align=center')
dot.node('h1', 'op = $f_1$ \\\\\ param = $\\textbf{W}_1$ \\\\\  state = $\\textbf{h}_1$', lblstyle='align=center')
dot.node('h2', 'op = $f_2$ \\\\\ param = $\\textbf{W}_2$ \\\\\ state = $\\textbf{h}_2$', lblstyle='align=center')
dot.node('h3', 'op = $f_3$ \\\\\ param = $\\textbf{W}_3$ \\\\\ state = $\\textbf{h}_3$', lblstyle='align=center')
dot.node('h4', 'op = $f_4$ \\\\\ param = $\\textbf{W}_4$ \\\\\ state = $\\textbf{h}_4$', lblstyle='align=center')
dot.node('h5', 'op = $f_5$ \\\\\ param = $\\textbf{W}_5$ \\\\\ state = $\hat{\\textbf{y}}$', lblstyle='align=center')

dot.edge('h0', 'h1')
dot.edge('h0', 'h2')
dot.edge('h1', 'h3')
dot.edge('h2', 'h4')
dot.edge('h3', 'h5')
dot.edge('h4', 'h5')

texcode = dot2tex.dot2tex(dot.source, format='tikz', texmode='raw', crop=True, autosize=True)
dir_path = os.path.dirname(os.path.realpath(__file__))
base_name = __file__.split(os.sep)[-1].replace('.py', '')
tex_file = os.path.join(dir_path, '{}.tex'.format(base_name))

with open(tex_file, 'w') as f:
	f.write(texcode)
	proc = Popen(['pdflatex', '-output-directory={}'.format(dir_path), tex_file])

proc.communicate()

rm_files = [os.path.join(dir_path,'fig1.aux'), os.path.join(dir_path,'fig1.tex'), os.path.join(dir_path,'fig1.log')]
call(['rm'] + rm_files)