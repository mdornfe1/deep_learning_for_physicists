\documentclass[12pt, letter]{report}

\usepackage{amsmath}    % need for subequations
\usepackage[pdftex]{graphicx}   % need for figures
\usepackage{verbatim}   % useful for program listings
\usepackage{xcolor}      % use if color is used in text
\usepackage{caption}  
\usepackage{subcaption}  % use for side-by-side figures
\usepackage{hyperref}   % use for hypertext links, including those to external documents and URLs
\usepackage{setspace}
\usepackage{listings}
\usepackage{rotating}
\usepackage{textcomp}
\usepackage{cleveref}

%\usepackage{breqn}
 
\urlstyle{same}

\doublespacing

\allowdisplaybreaks
\graphicspath{{figs/}}

\begin{document}
\chapter{FeedForward Neural Networks}
Suppose we have dataset represented by matrix $\textbf{X}$, where the rows of the matrix each represent an individual data point and the columns represent different features of the data. Each data point is assigned a discrete label (in case of classification) or a vector of continuous floats (in case of regression). In discrete case the labels are usually represented by a one hot encoded vector of binary numbers. In either case this target variable can be represented by the matrix $\textbf{Y}$, where the rows correspond to different data points and the columns correspond to different elements of the target vector. Let's denote an arbitrary row of the matrix $\textbf{X}$ by the symbol $\textbf{x}$ and the corresponding row of the matrix $\textbf{Y}$ by the symbol $\textbf{y}$. 

At it's most basic level the field of machine learning consists of different methods to learn a function $f$ that approximates $y$ given the appropriate $x$. More formally if $f$ is parameterized by the tensor $\textbf{W}$ can we found values for $\textbf{W}$ such that 
\begin{equation}
\hat{\textbf{y}} = f(\textbf{x}, \textbf{W})
\end{equation}
approximates the true target variables $\textbf{y}$. If $\hat{\textbf{y}}$ is close to $\textbf{y}$ it is reasonable to assume they different by only a small amount of noise, represented by vector $\boldsymbol{\epsilon}$. Thus we can write
\begin{equation}
\textbf{y} = f(\textbf{x}, \textbf{W}) + \boldsymbol{\epsilon}.
\end{equation}

In deep learning we impose a specific structure on the function $f$ and the parameters $\textbf{W}$, which we now call weights. We assume that the model can be represented by a computational graph. In the case of feedforward neural networks we will assume the graph is both directed and acyclic (DAG), when we discuss recurrent neural networks later the acyclic restriction will be relaxed. There are four types of nodes in the graph: input nodes, output nodes, hidden state nodes, and parameter nodes. Each hidden state is represented by a tensor (usually a one dimensional vector). Let's call the hidden state tensor associated with the $i^{th}$ node $\textbf{h}_i$. Each $\textbf{h}_i$ can be thought of as being produced by applying a function $f_i$ to the vectors stored in its parent nodes and that nodes parameters $\textbf{W}_i$. More formally 
\begin{equation}
\textbf{h}_i = f_i(P(\textbf{h}_i), \textbf{W}_i),
\end{equation}
where
\begin{equation}
P(\textbf{h}_i)=\{\textbf{h}_j| j \in \text{parent hidden state nodes of i}\}
\end{equation}
and $\textbf{W}_i$ is the subset of $\textbf{W}$ that parameterize the function $f_i$. In most software implementations each $\textbf{W}_i$ has their own node in the graph. However, since this node would have no other dependencies other than being a parent node to the node containing $\textbf{h}_i$, we can include it in the $\textbf{h}_i$ for sake of brevity and notational convenience.

\begin{figure}
\center
\includegraphics{./figs/fig1.pdf}
\caption{Deep learning model expressed in the form of a computational graph. Input nodes have as properties a state and child nodes, but no operation, parameters, or parent nodes. Hidden nodes have a state, an operation, parameters, child nodes, and parent nodes. Output nodes have a state, an operation, parameters, parent nodes, but no child nodes.}
\end{figure}

So far we discussed hidden state and parameter nodes. Input nodes contain our input data vector $\textbf{x}$. Output nodes contain our prediction vector $\hat{\textbf{y}}$. In practice they can also contain other quantities such as loss functions and other metrics, but we'll get to those later. 
\subsection{Multi Layer Perceptrons}
It would nice to have a closed form expression that expresses $\hat{\textbf{y}}$ in terms of $\textbf{x}$ and the internal hidden state vectors, functions, and parameters. In practice we can often make simplifying assumptions to make this easier to write. In the case of a multi layer perceptron (MLP) there is a simple sequential path to get from the input node, through the hidden states, to the output node. Thus, $P(\textbf{h}_i)=\textbf{h}_{i-1}$. For the MLP we can write
\begin{equation}
\textbf{h}_i=\begin{cases} 
      x & i=0 \\
      f_i(\textbf{h}_{i-1
      }, \textbf{W}_i) & 1 \leq i \leq N-1 \\
      \hat{y} & i=N 
   \end{cases}
\end{equation}

The MLP also assumes the function $f_i=\sigma(\textbf{W}_i \textbf{h}_{i-1})$, where $\sigma$ is known as an activation function (this will be discussed more later). In practice an offset $\textbf{b}_i$ is usually added to the argument of the $\sigma$ function. However, that can be though to be included in $\textbf{W}_i$ if we always append an entry of $1$ to the vector $\textbf{h}_{i_1}$. Thus we can write the above equation as 
\begin{equation}
\textbf{h}_i=\begin{cases} 
      x & i=0 \\
      \sigma(\textbf{W}_i \textbf{h}_{i-1}) & 1 \leq i \leq N-1 \\
      \hat{y} & i=N 
   \end{cases}
\end{equation}

\begin{figure}
\center
\includegraphics{mlp_graph.pdf}
\caption{Multilayer perceptron (MLP) expressed in the form of a computational graph. This MLP has two layers. Each layer consists of a matrix multiplication of the layers weights with the output of the previous layer followed by the application of an activation function $\sigma$. We denote the output of the first operation in a layer with the variable $\textbf{z}$ and the output of the second operation with the variable $\textbf{a}$. Note that an operation need not have parameters associated with it, as seen by the $\sigma$ operation nodes.}
\end{figure}

\section{Loss Functions and Output Layers}
So far we discussed how a deep learning model consists of a computational graph that takes an input $\textbf{x}$, performs a series of operations, and produces an output $\hat{\textbf{y}}$ that approximates a true label $\textbf{y}$. However, we have not yet discussed how good of an approximation $\hat{\textbf{y}}$ is for $\textbf{y}$. To do this we must introduce the concept of a loss function. In this section we will motivate and discuss common loss functions for classification and regression problems.

\subsection{Maximum A Posteriori Loss Function for Classification Problems}
In classification problems our task is to learn a model that can correctly assign a class $n$ out of $N$ possible classes. For concreteness we choose our class labels to be the integers of the set ${1,...,N}$. However any arbitrary label will work. With this in mind the $n^{th}$ element of the vector $\hat{\textbf{y}}=f(\textbf{x};\textbf{W})$ can be though of as the probability the data point belongs to class class $n$. In equation form, $\hat{y}_n=p(n|\textbf{W},\textbf{x})$. 


We will now use the principle of maximum a posteriori estimation (MAP) to derive a loss function for our classification problems. To do this we first use the definition of the categorical distribution to write the likelihood of a model with parameters $\textbf{W}$ generating data point $(\textbf{x}_l,\textbf{y}_l)$. 
\begin{equation}
L(\textbf{W}|X_{lm},Y_{ln})=\prod_{n=1}^{N}p(n|\textbf{W},X_{lm})^{Y_{ln}}.
\end{equation}
We can then use the assumption that data points for different values of $l$ are drawn independently from identical distributions to write
\begin{equation}
\label{eq:likelihood}
L(\textbf{W}|\textbf{X}, \textbf{Y})=\prod_{l=1}^{L}\prod_{n=1}^{N}p(n|\textbf{W},X_{lm})^{Y_{ln}}.
\end{equation}
Using Bayes' rule we can then write the posterior distribution
\begin{equation}
p(\textbf{W}|\textbf{X}, \textbf{Y})=\frac{L(\textbf{W}|\textbf{X}, \textbf{Y})p(\textbf{W})}{p(\textbf{Y})}
\end{equation}
We now apply the MAP principle to the above equation to obtain an estimator $\hat{\textbf{W}}$ for the parameters that allow our model to best approximate our data $(\textbf{X}, \textbf{Y})$. The MAP principle states that these parameters should be the ones that maximize the above posterior distribution.
\begin{equation}
\hat{\textbf{W}}=\underset{\textbf{W}}{\mathrm{argmax}} \; p(\textbf{W}|\textbf{X}, \textbf{Y})
\end{equation}
Thus,
\begin{equation}
\hat{\textbf{W}}=\underset{\textbf{W}}{\mathrm{argmax}} \; \frac{L(\textbf{W}|\textbf{X}, \textbf{Y})p(\textbf{W})}{p(\textbf{Y})}
\end{equation}
Since $p(\textbf{Y})$ does not depend on $\textbf{W}$ we can safely ignore it as it will not effect the value obtained from the maximization procedure.
\begin{equation}
\hat{\textbf{W}}=\underset{\textbf{W}}{\mathrm{argmax}} \; L(\textbf{W}|\textbf{X}, \textbf{Y})p(\textbf{W})
\end{equation}
Thus MAP is equivalent to maximizing the likelihood function subject to a prior assumption on the parameters $\textbf{W}$. There are many different choices we can make for prior distributions on these parameters. A common and basic choice to is to assume they're draw from a uniform distribution, in which $p(\textbf{W})$ becomes a constant and it will no longer effect the maximization procedure. We will discuss other priors we can place on the parameters when we discuss regularization. With the uniform prior assumption we get
\begin{equation}
\label{eq:mle}
\hat{\textbf{W}}=\underset{\textbf{W}}{\mathrm{argmax}} \; L(\textbf{W}|\textbf{X}, \textbf{Y})
\end{equation}
Thus we find that MAP principle with the assumption of a uniform prior on the parameters is equivalent to the principle of maximum likelihood estimation (MLE). This hopefully provides an intuitive justification for this method of obtaining the estimator $\hat{\textbf{W}}$; we are choosing the parameters that make our model most likely to reproduce our training data.

There are many algorithms by which we can maximize \cref{eq:mle}. However in practice direct application of MLE does not perform well for deep learning models. This is because there can often be millions of parameters, which cause this global optimization problem to become intractable. Gradient based optimization methods (e.g. gradient descent) have been shown to perform well on problems with large amounts of data and a large parameter space. With this in mind we will now convert \cref{eq:mle} to form that can be optimized by methods such as gradient descent. Taking the log of both sides of \cref{eq:likelihood}
\begin{equation}
\label{eq:log_likelihood}
\log L(\textbf{W}|\textbf{X}, \textbf{Y})=\log \prod_{l=1}^{L}\prod_{n=1}^{N}p(n|\textbf{W},X_{lm})^{Y_{ln}}
\end{equation}
We can do this without concern because the monotonocity of logarithms ensures that maximizing the log likelihood is equivalent to maximizing the likelihood. Using the properties of logarithms we can write the above equation as
\begin{equation}
\label{eq:log_likelihood2}
\log L(\textbf{W}|\textbf{X}, \textbf{Y})=\sum_{l=1}^{L}\sum_{n=1}^{N} Y_{ln} \log p(n|\textbf{W},X_{lm})
\end{equation}
The inner sum can be written using a dot product, $\sum_{n=1}^{N} Y_{ln} \log p(n|\textbf{W},X_{lm})=\textbf{y}_l \cdot \log \hat{\textbf{y}}_l$. Thus,
\begin{equation}
\label{eq:log_likelihood2}
\log L(\textbf{W}|\textbf{X}, \textbf{Y})=\sum_{l=1}^{L}\textbf{y}_l \cdot \log \hat{\textbf{y}}_l
\end{equation}
The negative of the quantity on the left is called the cross entropy of the two distributions $\textbf{Y}$ and $\hat{\textbf{Y}}$, that is the cross entropy of the true data distribution and the one learned by the model.
\begin{equation}
\label{eq:cross_entropy_loss}
H(\textbf{Y}, \hat{\textbf{Y}}) = - \sum_{l=1}^{L}\textbf{y}_l \cdot \log \hat{\textbf{y}}_l 
\end{equation}
Since $\log L(\textbf{W}|\textbf{X}, \textbf{Y})=-H(\textbf{Y}, \hat{\textbf{Y}})$ we can find the MAP estimator by solving the following optimization problem
\begin{equation}
\hat{\textbf{W}}=\underset{\textbf{W}}{\mathrm{argmin}} \; H(\textbf{Y}, \hat{\textbf{Y}}(\textbf{W}, \textbf{X}))
\end{equation}
This optimization problem can be solved via methods such as gradient descent since the cross entropy function and deep learning models are differentiable. Thus, \cref{eq:cross_entropy_loss} is the loss function we've been looking for. By minimizing that we can find an optimal set of weights for our model.

We can also think of this result from an information theory perspective. The Kullback-Leibler divergence (KL divergence) between two distributions is defined as 
\begin{equation}
D_\text{KL}(\textbf{Y} \| \hat{\textbf{Y}}) = -\sum_l {\textbf{y}_l\, \log\left(\frac{\hat{\textbf{y}}_l}{\textbf{y}_l}\right)}
\end{equation}
It can be thought of a measure of the distance between the true distribution $\textbf{Y}$ and the estimated distribution $\hat{\textbf{Y}}$. From the definition of cross entropy \cref{eq:cross_entropy_loss} and the definition of the entropy of a distribution $H(\textbf{Y)}=-\sum_l \textbf{y}_l \log \textbf{y}_l$, we can write the KL divergence as
\begin{equation}
D_\text{KL}(\textbf{Y} \| \hat{\textbf{Y}}) = H(\textbf{Y}, \hat{\textbf{Y}}) - H(\textbf{Y)}
\end{equation}
Thus, by choosing parameters $\hat{\textbf{W}}$ that minimizes the cross entropy we are also minimizing the KL divergence. This means we can think of the MAP estimator as given us a model that learns a distribution $\hat{\textbf{Y}}$ that is "close" to the real distribution $\textbf{Y}$.

\subsection{Output Layer for Classification Problems}
Since our model is being used to estimate the parameters of the categorical distribution we have to put an extra restriction on the activation function of the output layer, namely we have to ensure that the entries of the output vector form a probability distribution an sum to one. The most common way of doing this is using the softmax function for the activation function on the output layer. More formally we choose
\begin{equation}
\sigma (\textbf{h})_m = \frac{\exp(h_m)}{\sum_{m'}\exp(h_{m'})}
\end{equation}
as the operation for the output node, where $\textbf{h}$ is the input tensor to that node.
\end{document}